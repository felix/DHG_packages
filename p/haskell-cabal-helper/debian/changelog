haskell-cabal-helper (0.8.0.2-2) unstable; urgency=medium

  [ Clint Adams ]
  * Set Rules-Requires-Root to no.

  [ Ilias Tsitsimpis ]
  * Bump debhelper compat level to 10
  * Remove build dependency on libghc-mtl-dev (provided by ghc-8.4.3)

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Mon, 01 Oct 2018 13:33:22 +0300

haskell-cabal-helper (0.8.0.2-1) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.
  * New upstream release

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 15:27:03 -0400

haskell-cabal-helper (0.7.3.0-3) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS

  [ Daniel Gröber ]
  * Add patch to disable $libexec hacks.  closes: #878112.

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.3.

 -- Clint Adams <clint@debian.org>  Fri, 29 Dec 2017 13:17:51 -0500

haskell-cabal-helper (0.7.3.0-2) unstable; urgency=medium

  * Fix path of cabal-helper-wrapper.  closes: #865014.

 -- Clint Adams <clint@debian.org>  Sun, 18 Jun 2017 20:06:39 -0400

haskell-cabal-helper (0.7.3.0-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Sat, 17 Jun 2017 13:54:07 -0400

haskell-cabal-helper (0.7.2.0-3) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:32:44 -0400

haskell-cabal-helper (0.7.2.0-2) experimental; urgency=medium

  * Handle cabal-helper-wrapper being renamed to
    cabal-helper-wrapper-v0.7.

 -- Clint Adams <clint@debian.org>  Mon, 24 Oct 2016 08:42:02 -0400

haskell-cabal-helper (0.7.2.0-1) experimental; urgency=medium

  * New upstream version.

 -- Clint Adams <clint@debian.org>  Sun, 16 Oct 2016 16:40:31 -0400

haskell-cabal-helper (0.6.3.1-1) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sat, 28 May 2016 13:38:56 +0200

haskell-cabal-helper (0.6.3.0-1) unstable; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)

  [ Clint Adams ]
  * New upstream release

 -- Clint Adams <clint@debian.org>  Wed, 25 May 2016 13:05:01 -0400

haskell-cabal-helper (0.6.2.0-4) unstable; urgency=high

  * Depend on haskell-devscripts >= 0.10.2.2 which sets the correct
    libexecdir. (Closes: #810105)

 -- Sven Bartscher <sven.bartscher@weltraumschlangen.de>  Fri, 08 Jan 2016 15:27:59 +0100

haskell-cabal-helper (0.6.2.0-3) unstable; urgency=high

  * Add binary-or-shlib-defines-rpath lintian override
  * Added dependencies on shared libraries

 -- Sven Bartscher <sven.bartscher@weltraumschlangen.de>  Wed, 06 Jan 2016 16:55:06 +0100

haskell-cabal-helper (0.6.2.0-2) unstable; urgency=high

  * Actually install the cabal-helper-wrapper executable to /usr/lib
    (Closes: #809763)

 -- Sven Bartscher <sven.bartscher@weltraumschlangen.de>  Mon, 04 Jan 2016 16:23:49 +0100

haskell-cabal-helper (0.6.2.0-1) unstable; urgency=medium

  * New upstream release

 -- Sven Bartscher <sven.bartscher@weltraumschlangen.de>  Fri, 25 Dec 2015 12:36:37 +0100

haskell-cabal-helper (0.5.1.0-4) unstable; urgency=medium

  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:54:10 -0500

haskell-cabal-helper (0.5.1.0-3) experimental; urgency=low

  [ Joachim Breitner ]
  * Add Uploaders field, which I accidentally dropped

 -- Sven Bartscher <sven.bartscher@weltraumschlangen.de>  Wed, 30 Sep 2015 18:03:01 +0200

haskell-cabal-helper (0.5.1.0-2) experimental; urgency=medium

  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:27:07 +0200

haskell-cabal-helper (0.5.1.0-1) unstable; urgency=low

  * Initial release

 -- Sven Bartscher <sven.bartscher@weltraumschlangen.de>  Sun, 16 Aug 2015 19:05:16 +0200
