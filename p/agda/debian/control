Source: agda
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 alex,
 cdbs,
 cpphs (>= 1.20.2),
 debhelper (>= 10),
 dh-elpa,
 ghc (>= 8.4.3),
 happy,
 haskell-devscripts (>= 0.13),
 ghc-prof,
 libghc-edison-core-dev,
 libghc-edison-core-prof,
 libghc-aeson-dev (>= 0.11.3.0),
 libghc-aeson-dev (<< 1.5),
 libghc-aeson-prof,
 libghc-async-dev (>= 2.2),
 libghc-async-dev (<< 2.3),
 libghc-blaze-html-dev (>= 0.8),
 libghc-blaze-html-dev (<< 0.10),
 libghc-boxes-dev (>= 0.1.3),
 libghc-boxes-dev (<< 0.2),
 libghc-boxes-dev (>= 0.1.3),
 libghc-data-hash-dev (<< 0.3),
 libghc-data-hash-dev (>= 0.2.0.0),
 libghc-edit-distance-dev (<< 0.3),
 libghc-equivalence-dev (>= 0.3.2),
 libghc-equivalence-dev (<< 0.4),
 libghc-equivalence-prof,
 libghc-exceptions-dev (>= 0.8),
 libghc-exceptions-dev (<< 0.11),
 libghc-exceptions-prof,
 libghc-geniplate-mirror-dev (>= 0.6.0.6),
 libghc-geniplate-mirror-dev (<< 0.8),
 libghc-gitrev-dev (>= 1.3.1),
 libghc-gitrev-dev (<< 2.0),
 libghc-gitrev-dev (>= 1.2),
 libghc-hashable-dev (<< 1.3),
 libghc-hashtables-dev (>= 1.2.0.2),
 libghc-hashtables-dev (<< 1.3),
 libghc-ieee754-dev (>= 0.7.8),
 libghc-ieee754-dev (<< 0.9),
 libghc-murmur-hash-dev (>= 0.1),
 libghc-murmur-hash-dev (<< 0.2),
 libghc-regex-tdfa-dev (>= 1.2.2),
 libghc-regex-tdfa-dev (<< 1.3),
 libghc-regex-tdfa-prof,
 libghc-split-dev (>= 0.2.0.0),
 libghc-split-dev (<< 0.2.3.4),
 libghc-split-prof,
 libghc-strict-dev (>= 0.3.2),
 libghc-strict-dev (<< 0.4),
 libghc-unordered-containers-dev (>= 0.2.5.0),
 libghc-unordered-containers-dev (<< 0.3),
 libghc-uri-encode-dev (>= 1.5.0.4),
 libghc-uri-encode-dev (<< 1.6),
 libghc-zlib-dev (>= 0.4.0.1),
 libghc-zlib-dev (<< 0.7),
 alex,
 happy,
Build-Depends-Indep: ghc-doc,
 libghc-edison-core-doc,
 libghc-aeson-doc,
 libghc-async-doc,
 libghc-blaze-html-doc,
 libghc-boxes-doc,
 libghc-data-hash-doc,
 libghc-edit-distance-doc,
 libghc-equivalence-doc,
 libghc-exceptions-doc,
 libghc-geniplate-mirror-doc,
 libghc-gitrev-doc,
 libghc-hashable-doc,
 libghc-hashtables-doc,
 libghc-ieee754-doc,
 libghc-murmur-hash-doc,
 libghc-regex-tdfa-doc,
 libghc-split-doc,
 libghc-strict-doc,
 libghc-unordered-containers-doc,
 libghc-uri-encode-doc,
 libghc-zlib-doc,
Standards-Version: 4.4.0
Homepage: http://wiki.portal.chalmers.se/agda/
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/agda
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/agda]

Package: agda
Architecture: all
Section: misc
Depends:
 agda-bin,
 agda-stdlib,
 agda-stdlib-doc,
 elpa-agda2-mode,
 libghc-agda-dev,
 ${misc:Depends},
Description: dependently typed functional programming language
 Agda is a dependently typed functional programming language: It has inductive
 families, which are like Haskell's GADTs, but they can be indexed by values and
 not just types. It also has parameterised modules, mixfix operators, Unicode
 characters, and an interactive Emacs interface (the type checker can assist in
 the development of your code).
 .
 Agda is also a proof assistant: It is an interactive system for writing and
 checking proofs. Agda is based on intuitionistic type theory, a foundational
 system for constructive mathematics developed by the Swedish logician Per
 Martin-Löf. It has many similarities with other proof assistants based on
 dependent types, such as Coq, Epigram and NuPRL.
 .
 This is a meta package which provides Agda's emacs mode, executable, standard
 library and its documentation.

Package: agda-bin
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 libghc-agda-dev (<< ${source:Upstream-Version}+~),
 libghc-agda-dev (>= ${source:Upstream-Version}),
 ${haskell:Recommends},
Suggests:
 elpa-agda2-mode,
 ${haskell:Suggests},
Description: commandline interface to Agda
 Agda is a dependently typed functional programming language: It has inductive
 families, which are like Haskell's GADTs, but they can be indexed by values and
 not just types. It also has parameterised modules, mixfix operators, Unicode
 characters, and an interactive Emacs interface (the type checker can assist in
 the development of your code).
 .
 Agda is also a proof assistant: It is an interactive system for writing and
 checking proofs. Agda is based on intuitionistic type theory, a foundational
 system for constructive mathematics developed by the Swedish logician Per
 Martin-Löf. It has many similarities with other proof assistants based on
 dependent types, such as Coq, Epigram and NuPRL.
 .
 This package provides a command-line program for type-checking and compiling
 Agda programs. The program can also generate hyperlinked, highlighted HTML
 files from Agda sources.

Package: elpa-agda2-mode
Section: editors
Architecture: all
Depends:
 agda-bin (<< ${source:Version}.1~),
 agda-bin (>= ${source:Version}),
 libghc-agda-dev (<< ${source:Version}.1~),
 libghc-agda-dev (>= ${source:Version}),
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Built-Using: ${misc:Built-Using}
Recommends:
 emacs (>= 46.0),
Enhances:
 emacs,
 emacs24,
Breaks:
 agda-mode (<< 2.5.1),
Provides:
 agda-mode,
Description: dependently typed functional programming language — emacs mode
 Agda is a dependently typed functional programming language: It has inductive
 families, which are like Haskell's GADTs, but they can be indexed by values and
 not just types. It also has parameterised modules, mixfix operators, Unicode
 characters, and an interactive Emacs interface (the type checker can assist in
 the development of your code).
 .
 Agda is also a proof assistant: It is an interactive system for writing and
 checking proofs. Agda is based on intuitionistic type theory, a foundational
 system for constructive mathematics developed by the Swedish logician Per
 Martin-Löf. It has many similarities with other proof assistants based on
 dependent types, such as Coq, Epigram and NuPRL.
 .
 This package contains the emacs interactive development mode for Agda. This
 mode is the preferred way to write Agda code, and offers features such as
 iterative development, refinement, case analysis and so on.

Package: agda-mode
Architecture: all
Section: oldlibs
Depends:
 ${misc:Depends},
Description: transitional dummy package for elpa-agda2-mode
 agda-mode has been ELPAfied.  See the elpa-agda2-mode package.  This
 transitional package is safe to remove.

Package: libghc-agda-dev
Architecture: any
Section: haskell
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: dependently typed functional programming language${haskell:ShortBlurb}
 Agda is a dependently typed functional programming language: It has inductive
 families, which are like Haskell's GADTs, but they can be indexed by values and
 not just types. It also has parameterised modules, mixfix operators, Unicode
 characters, and an interactive Emacs interface (the type checker can assist in
 the development of your code).
 .
 Agda is also a proof assistant: It is an interactive system for writing and
 checking proofs. Agda is based on intuitionistic type theory, a foundational
 system for constructive mathematics developed by the Swedish logician Per
 Martin-Löf. It has many similarities with other proof assistants based on
 dependent types, such as Coq, Epigram and NuPRL.
 .
 ${haskell:Blurb}

Package: libghc-agda-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 haskell-agda-doc (<< 2.2.6-5),
Provides:
 haskell-agda-doc,
Replaces:
 haskell-agda-doc,
Description: dependently typed functional programming language${haskell:ShortBlurb}
 Agda is a dependently typed functional programming language: It has inductive
 families, which are like Haskell's GADTs, but they can be indexed by values and
 not just types. It also has parameterised modules, mixfix operators, Unicode
 characters, and an interactive Emacs interface (the type checker can assist in
 the development of your code).
 .
 Agda is also a proof assistant: It is an interactive system for writing and
 checking proofs. Agda is based on intuitionistic type theory, a foundational
 system for constructive mathematics developed by the Swedish logician Per
 Martin-Löf. It has many similarities with other proof assistants based on
 dependent types, such as Coq, Epigram and NuPRL.
 .
 ${haskell:Blurb}
