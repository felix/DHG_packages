haskell-werewolf (1.5.2.0-4) unstable; urgency=medium

  * Relax dependency on aeson and template-haskell

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Wed, 07 Nov 2018 15:49:14 +0200

haskell-werewolf (1.5.2.0-3) unstable; urgency=medium

  * Remove build dependency on libghc-mtl-dev (provided by ghc-8.4.3)
  * Remove build dependency on libghc-text-dev (provided by ghc-8.4.3)

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Mon, 01 Oct 2018 13:47:47 +0300

haskell-werewolf (1.5.2.0-2) unstable; urgency=medium

  [ Clint Adams ]
  * Set Rules-Requires-Root to no.

  [ Ilias Tsitsimpis ]
  * Bump debhelper compat level to 10

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 30 Sep 2018 21:25:20 +0300

haskell-werewolf (1.5.2.0-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Wed, 18 Apr 2018 08:10:38 -0400

haskell-werewolf (1.5.1.1-9) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 20:05:10 -0400

haskell-werewolf (1.5.1.1-8) unstable; urgency=medium

  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1
  * Update build-deps to build with newer version of haskell-aeson
    Thanks to Steve Langasek for the patch (Closes: #881017)

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Tue, 07 Nov 2017 11:26:06 +0200

haskell-werewolf (1.5.1.1-7) unstable; urgency=medium

  * Patch for newer aeson and lens.  closes: #868629.

 -- Clint Adams <clint@debian.org>  Wed, 19 Jul 2017 18:52:37 -0400

haskell-werewolf (1.5.1.1-6) unstable; urgency=medium

  * Fix patch for newer extra.  closes: #867872.

 -- Clint Adams <clint@debian.org>  Mon, 10 Jul 2017 07:56:03 -0400

haskell-werewolf (1.5.1.1-5) unstable; urgency=medium

  * Patch for newer extra and MonadRandom.  closes: #867842.

 -- Clint Adams <clint@debian.org>  Sun, 09 Jul 2017 16:49:46 -0400

haskell-werewolf (1.5.1.1-4) unstable; urgency=medium

  * Team upload.
  * fix test build with upstream patch

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 21 Jun 2017 08:49:48 +0200

haskell-werewolf (1.5.1.1-3) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:36:59 -0400

haskell-werewolf (1.5.1.1-2) experimental; urgency=medium

  * Set build locale to C.UTF-8.

 -- Clint Adams <clint@debian.org>  Wed, 19 Oct 2016 13:12:01 -0400

haskell-werewolf (1.5.1.1-1) experimental; urgency=medium

  * New upstream version.

 -- Clint Adams <clint@debian.org>  Sat, 08 Oct 2016 00:44:41 -0400

haskell-werewolf (1.0.2.2-1) unstable; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)

  [ Joachim Breitner ]
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Mon, 30 May 2016 17:19:22 +0200

haskell-werewolf (0.5.1.0-2) unstable; urgency=medium

  * Only run tests on platforms with SMP support.  This is not
    a good solution.  closes: #819995.

 -- Clint Adams <clint@debian.org>  Sun, 17 Apr 2016 15:46:03 -0400

haskell-werewolf (0.5.1.0-1) unstable; urgency=low

  * Initial release.

 -- Clint Adams <clint@debian.org>  Sun, 03 Apr 2016 19:47:43 -0400
