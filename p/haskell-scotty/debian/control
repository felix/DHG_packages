Source: haskell-scotty
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Clint Adams <clint@debian.org>
Priority: optional
Section: haskell
Build-Depends: debhelper (>= 10),
 haskell-devscripts-minimal | haskell-devscripts (>= 0.8),
 cdbs,
 ghc,
 ghc-prof,
 libghc-aeson-dev (>= 0.6.2.1),
 libghc-aeson-dev (<< 1.5),
 libghc-aeson-prof (>= 0.6.2.1),
 libghc-aeson-prof (<< 1.5),
 libghc-blaze-builder-dev (>= 0.3.3.0),
 libghc-blaze-builder-dev (<< 0.5),
 libghc-blaze-builder-prof (>= 0.3.3.0),
 libghc-blaze-builder-prof (<< 0.5),
 libghc-case-insensitive-dev (>= 1.0.0.1),
 libghc-case-insensitive-dev (<< 1.3),
 libghc-case-insensitive-prof (>= 1.0.0.1),
 libghc-case-insensitive-prof (<< 1.3),
 libghc-data-default-class-dev (>= 0.0.1),
 libghc-data-default-class-dev (<< 0.2),
 libghc-data-default-class-prof (>= 0.0.1),
 libghc-data-default-class-prof (<< 0.2),
 libghc-exceptions-dev (>= 0.7),
 libghc-exceptions-dev (<< 0.11),
 libghc-exceptions-prof (>= 0.7),
 libghc-exceptions-prof (<< 0.11),
 libghc-http-types-dev (>= 0.8.2),
 libghc-http-types-dev (<< 0.13),
 libghc-http-types-prof (>= 0.8.2),
 libghc-http-types-prof (<< 0.13),
 libghc-monad-control-dev (>= 1.0.0.3),
 libghc-monad-control-dev (<< 1.1),
 libghc-monad-control-prof (>= 1.0.0.3),
 libghc-monad-control-prof (<< 1.1),
 libghc-network-dev (>= 2.6.0.2),
 libghc-network-dev (<< 3.2),
 libghc-network-prof (>= 2.6.0.2),
 libghc-network-prof (<< 3.2),
 libghc-regex-compat-dev (>= 0.95.1),
 libghc-regex-compat-dev (<< 0.96),
 libghc-regex-compat-prof (>= 0.95.1),
 libghc-regex-compat-prof (<< 0.96),
 libghc-transformers-base-dev (>= 0.4.1),
 libghc-transformers-base-dev (<< 0.5),
 libghc-transformers-base-prof (>= 0.4.1),
 libghc-transformers-base-prof (<< 0.5),
 libghc-transformers-compat-dev (>= 0.4),
 libghc-transformers-compat-dev (<< 0.7),
 libghc-transformers-compat-prof (>= 0.4),
 libghc-transformers-compat-prof (<< 0.7),
 libghc-wai-dev (>= 3.0.0),
 libghc-wai-dev (<< 3.3),
 libghc-wai-prof (>= 3.0.0),
 libghc-wai-prof (<< 3.3),
 libghc-wai-extra-dev (>= 3.0.0),
 libghc-wai-extra-dev (<< 3.1),
 libghc-wai-extra-prof (>= 3.0.0),
 libghc-wai-extra-prof (<< 3.1),
 libghc-warp-dev (>= 3.0.13),
 libghc-warp-dev (<< 3.3),
 libghc-warp-prof (>= 3.0.13),
 libghc-warp-prof (<< 3.3),
 libghc-async-dev,
 libghc-async-prof,
 libghc-data-default-class-dev,
 libghc-data-default-class-prof,
 libghc-hspec-dev (>= 2),
 libghc-hspec-dev (<< 3),
 libghc-hspec-prof (>= 2),
 libghc-hspec-prof (<< 3),
 libghc-hspec-wai-dev (>= 0.6.3),
 libghc-hspec-wai-prof (>= 0.6.3),
 libghc-http-types-dev,
 libghc-http-types-prof,
 libghc-lifted-base-dev,
 libghc-lifted-base-prof,
 libghc-network-dev,
 libghc-network-prof,
 libghc-wai-dev,
 libghc-wai-prof,
Build-Depends-Indep: ghc-doc,
 libghc-aeson-doc,
 libghc-blaze-builder-doc,
 libghc-case-insensitive-doc,
 libghc-data-default-class-doc,
 libghc-exceptions-doc,
 libghc-http-types-doc,
 libghc-monad-control-doc,
 libghc-network-doc,
 libghc-regex-compat-doc,
 libghc-transformers-base-doc,
 libghc-transformers-compat-doc,
 libghc-wai-doc,
 libghc-wai-extra-doc,
 libghc-warp-doc,
Standards-Version: 4.4.0
Homepage: https://github.com/scotty-web/scotty
X-Description: Haskell web framework inspired by Ruby's Sinatra
 A Haskell web framework inspired by Ruby's Sinatra, using WAI and Warp.
 .
 Scotty is the cheap and cheerful way to write RESTful, declarative web
 applications.
 .
 * A page is as simple as defining the verb, url pattern, and Text content.
 .
 * It is template-language agnostic. Anything that returns a Text value will do.
 .
 * Conforms to WAI Application interface.
 .
 * Uses very fast Warp webserver by default.
 .
 As for the name: Sinatra + Warp = Scotty.

Package: libghc-scotty-dev
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-scotty-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-scotty-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
