haskell-platform (2014.2.0.0.debian8) unstable; urgency=medium

  * Remove build dependency on libghc-parsec3-dev (provided by ghc-
    8.4.3)

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Wed, 03 Oct 2018 12:06:14 +0300

haskell-platform (2014.2.0.0.debian7) unstable; urgency=medium

  * Remove build dependency on libghc-mtl-dev (provided by ghc-8.4.3)
  * Remove build dependency on libghc-text-dev (provided by ghc-8.4.3)
  * Remove build dependency on libghc-stm-dev (provided by ghc-8.4.3)

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Mon, 01 Oct 2018 17:50:18 +0300

haskell-platform (2014.2.0.0.debian6) unstable; urgency=medium

  [ Clint Adams ]
  * Set Rules-Requires-Root to no.

  [ Ilias Tsitsimpis ]
  * Bump debhelper compat level to 10

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 30 Sep 2018 21:17:47 +0300

haskell-platform (2014.2.0.0.debian5) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Declare compliance with Debian policy 4.1.1
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 20:05:00 -0400

haskell-platform (2014.2.0.0.debian4) unstable; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)
  * Convert `debian/copyright' to dep5 format

  [ Clint Adams ]
  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:35:30 -0400

haskell-platform (2014.2.0.0.debian3) unstable; urgency=medium

  [ Joachim Breitner ]
  * Bump standards-version to 3.9.6

  [ Clint Adams ]
  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:54:55 -0500

haskell-platform (2014.2.0.0.debian2) unstable; urgency=medium

  * Fix dependencies of haskell-platform-doc. Thanks to Aaron M. Ucko for
    noticing. Closes: 784369

 -- Joachim Breitner <nomeata@debian.org>  Tue, 05 May 2015 23:05:49 +0200

haskell-platform (2014.2.0.0.debian1) unstable; urgency=medium

  * Upgrade to 2014.2.0.0
  * Drop version constraints. They were not very useful, as every Debian
    release ships with only one version of each package anyways, and we do not
    necessarily follow the version specified by the platform.

 -- Joachim Breitner <nomeata@debian.org>  Mon, 27 Apr 2015 14:17:24 +0200

haskell-platform (2013.2.0.0.debian12) unstable; urgency=medium

  * Bump happy

 -- Joachim Breitner <nomeata@debian.org>  Fri, 08 Aug 2014 12:39:10 +0200

haskell-platform (2013.2.0.0.debian11) unstable; urgency=medium

  * Bump cgi, haskell-src, parallel (minor upgrades)

 -- Joachim Breitner <nomeata@debian.org>  Wed, 06 Aug 2014 09:12:42 +0200

haskell-platform (2013.2.0.0.debian10) unstable; urgency=medium

  * "+" in version numbers still doesn't do what I think it does.

 -- Joachim Breitner <nomeata@debian.org>  Sun, 03 Aug 2014 17:04:37 +0200

haskell-platform (2013.2.0.0.debian9) unstable; urgency=medium

  * Allow newer QuickCheck and unordered-containers

 -- Joachim Breitner <nomeata@debian.org>  Sat, 02 Aug 2014 10:02:20 +0200

haskell-platform (2013.2.0.0.debian8) unstable; urgency=medium

  * Allow newer fgl

 -- Joachim Breitner <nomeata@debian.org>  Thu, 17 Jul 2014 10:29:56 +0200

haskell-platform (2013.2.0.0.debian7) unstable; urgency=medium

  * Fix upper bound syntax for cabal-install (Closes: #752728)

 -- Joachim Breitner <nomeata@debian.org>  Wed, 25 Jun 2014 15:44:11 -0700

haskell-platform (2013.2.0.0.debian6) unstable; urgency=medium

  * Allow newer HTTP, for its bugfixes.

 -- Joachim Breitner <nomeata@debian.org>  Mon, 23 Jun 2014 19:19:37 -0700

haskell-platform (2013.2.0.0.debian5) unstable; urgency=medium

  * Bump async to allow 2.0.1.5, and allow cabal-install 1.20

 -- Joachim Breitner <nomeata@debian.org>  Sat, 21 Jun 2014 22:59:47 -0700

haskell-platform (2013.2.0.0.debian4) unstable; urgency=medium

  * Bump happy to inlucde 1.19.3 (bugfix release)

 -- Joachim Breitner <nomeata@debian.org>  Wed, 12 Mar 2014 08:31:29 +0100

haskell-platform (2013.2.0.0.debian3) unstable; urgency=medium

  * bump case-insensitive, because we happened to update it. (Closes: #732597)

 -- Joachim Breitner <nomeata@debian.org>  Fri, 20 Dec 2013 15:17:51 +0100

haskell-platform (2013.2.0.0.debian2) unstable; urgency=low

  * bump hashable because #725577 made us upload a newer version

 -- Louis Bettens <louis@bettens.info>  Mon, 14 Oct 2013 10:04:28 +0200

haskell-platform (2013.2.0.0.debian1) unstable; urgency=low

  * Bump alex and happy to include versions that are able to build the
    upcoming GHC 7.8

 -- Joachim Breitner <nomeata@debian.org>  Fri, 20 Sep 2013 10:17:56 +0200

haskell-platform (2013.2.0.0) unstable; urgency=low

  * No longer rc, this is the “final” package.

 -- Joachim Breitner <nomeata@debian.org>  Sun, 16 Jun 2013 23:56:18 +0200

haskell-platform (2013.2.0.0~rc3) unstable; urgency=low

  * Restrict to QuickCheck 2.6 as specified by the platform.

 -- Joachim Breitner <nomeata@debian.org>  Sun, 16 Jun 2013 23:20:29 +0200

haskell-platform (2013.2.0.0~rc2) unstable; urgency=low

  * Restrict to alex 3.0.5 as specified by the platform.

 -- Joachim Breitner <nomeata@debian.org>  Thu, 30 May 2013 00:10:13 +0200

haskell-platform (2013.2.0.0~rc1) unstable; urgency=low

  * Fixes section of haskell-platform-prof (Closes: #678369)
  * Begin work on 2013.2.0.0. Differences:
    + alex and quichcheck not yet updatd
    + cgi at 3001.1.8.3 instead of 3001.1.7.5
  * Bump standards and compat

 -- Joachim Breitner <nomeata@debian.org>  Wed, 29 May 2013 13:42:35 +0200

haskell-platform (2012.2.0.0) unstable; urgency=low

  * Final release, no changes since rc2.debian1

 -- Joachim Breitner <nomeata@debian.org>  Sun, 03 Jun 2012 19:38:15 +0200

haskell-platform (2012.2.0.0~rc2.debian1) unstable; urgency=low

  * Bump GLUT, we upgraded too early and upstream reverted to 2.1.2.1, but
    lets just stick with 2.1.2.2.

 -- Joachim Breitner <nomeata@debian.org>  Wed, 30 May 2012 12:51:02 +0200

haskell-platform (2012.2.0.0~rc2) unstable; urgency=low

  * Bump xhtml, mtl and transformers dependency.

 -- Joachim Breitner <nomeata@debian.org>  Fri, 25 May 2012 09:40:59 +0200

haskell-platform (2012.1.0.0~debian4) unstable; urgency=low

  * Got the network and http dependencies wrong.

 -- Joachim Breitner <nomeata@debian.org>  Mon, 14 May 2012 15:29:57 +0200

haskell-platform (2012.1.0.0~debian3) unstable; urgency=low

  * Darn, forgot to pull before I upload.
  * debian/source/format 3.0 (native)
  * Also bump text to the 2012.2.0.0 version.

 -- Joachim Breitner <nomeata@debian.org>  Sun, 13 May 2012 11:41:10 +0200

haskell-platform (2012.1.0.0~debian2) unstable; urgency=low

  * Bump stm, network, syb to converge to the version set that will be the
    official platform 2012.2.0.0.

 -- Joachim Breitner <nomeata@debian.org>  Sun, 13 May 2012 11:38:16 +0200

haskell-platform (2012.1.0.0~debian1) unstable; urgency=low

  * Another unofficial release, supporting GHC 7.4.1's cabal version.

 -- Iain Lane <laney@debian.org>  Wed, 18 Apr 2012 22:22:02 +0100

haskell-platform (2012.1.0.0~debian) unstable; urgency=low

  * Unofficial release, to be able to install a platform on Debian with GHC
    7.4.1 (Closes: #662646)

 -- Joachim Breitner <nomeata@debian.org>  Tue, 13 Mar 2012 15:07:43 +0100

haskell-platform (2011.4.0.0) unstable; urgency=high

  * Upstream has released, no other changes to the package. Urgency high to
    not slow down its transition to testing.

 -- Joachim Breitner <nomeata@debian.org>  Sat, 17 Dec 2011 22:39:57 +0100

haskell-platform (2011.3.0.0~pre.5) unstable; urgency=low

  * Upgrade cgi to 3001.1.8.2, as this is the version that ended up in debian.

 -- Joachim Breitner <nomeata@debian.org>  Sun, 30 Oct 2011 17:56:40 +0100

haskell-platform (2011.3.0.0~pre.4) unstable; urgency=low

  * Fix short description typo (Closes: #636387)
  * New upcoming platform release, with tentative versions. We use newer
    versions of network and of alex. (Closes: 643902)

 -- Joachim Breitner <nomeata@debian.org>  Thu, 13 Oct 2011 23:32:20 +0200

haskell-platform (2011.2.0.1.3) unstable; urgency=low

  * Bump ghc dependency to 7.0.4 (minor bugfix release)
  * Add Recommends from -dev to platform package.

 -- Joachim Breitner <nomeata@debian.org>  Tue, 02 Aug 2011 20:01:16 +0200

haskell-platform (2011.2.0.1.2) unstable; urgency=low

  * Fix quickcheck2 dependency. 2.4.1.1 was uploaded to Debian despite being
    newer than specified in 2011.2.0.1, so lets use that. (Closes: #626979)

 -- Joachim Breitner <nomeata@debian.org>  Thu, 26 May 2011 17:17:31 +0200

haskell-platform (2011.2.0.1.1) unstable; urgency=low

  * New minor platform release, bumps ghc and text dependency.

 -- Joachim Breitner <nomeata@debian.org>  Mon, 11 Apr 2011 19:55:35 +0530

haskell-platform (2011.2.0.0.3) unstable; urgency=low

  * Add haskell-platform-prof meta package (Closes: #620131)

 -- Joachim Breitner <nomeata@debian.org>  Wed, 30 Mar 2011 23:10:49 +0530

haskell-platform (2011.2.0.0.2) unstable; urgency=low

  * Use saner HTTP version number

 -- Joachim Breitner <nomeata@debian.org>  Wed, 30 Mar 2011 10:34:14 +0530

haskell-platform (2011.2.0.0.1) unstable; urgency=low

  [ Joachim Breitner ]
  * Priority extra

  [ Marco Silva ]
  * Use ghc instead of ghc6

  [ Joachim Breitner ]
  * New platform version

 -- Joachim Breitner <nomeata@debian.org>  Tue, 29 Mar 2011 14:08:37 +0530

haskell-platform (2010.1.0.0.1) unstable; urgency=low

  * New platform version
  * Suggest haskell-platform-doc, so that users are more likely to find this
    package.
  * Add a README.Debian file.
  * Fix version numbers in dependencies, to make sure there is no mix-up in
    stable, i.e. there is always exactly one complete haskell-platform in
    stable.
  * Annotate divergences from upstream platform in debian/control

 -- Joachim Breitner <nomeata@debian.org>  Sun, 01 Nov 2009 12:56:13 +0100

haskell-platform (2009.2.0.2.1) unstable; urgency=low

  * Initial release

 -- Joachim Breitner <nomeata@debian.org>  Sat, 10 Oct 2009 13:25:47 +0200
