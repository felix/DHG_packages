Source: haskell-esqueleto
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Clint Adams <clint@debian.org>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends: debhelper (>= 10),
 haskell-devscripts (>= 0.13),
 cdbs,
 ghc (>= 8.4.3),
 ghc-prof,
 libghc-blaze-html-dev,
 libghc-blaze-html-prof,
 libghc-conduit-dev (>= 1.3),
 libghc-conduit-prof,
 libghc-monad-logger-dev,
 libghc-monad-logger-prof,
 libghc-persistent-dev (>= 2.8.0),
 libghc-persistent-dev (<< 2.10),
 libghc-persistent-prof,
 libghc-resourcet-dev (>= 1.2),
 libghc-resourcet-prof,
 libghc-tagged-dev (>= 0.2),
 libghc-tagged-prof,
 libghc-unliftio-dev,
 libghc-unliftio-prof,
 libghc-unordered-containers-dev (>= 0.2),
 libghc-unordered-containers-prof,
 libghc-hunit-dev,
 libghc-quickcheck2-dev,
 libghc-hspec-dev (>= 1.8),
 libghc-persistent-dev,
 libghc-persistent-sqlite-dev (>= 2.1.3),
 libghc-persistent-template-dev (>= 2.1),
Standards-Version: 4.4.0
Homepage: https://github.com/bitemyapp/esqueleto
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-esqueleto
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-esqueleto]
X-Description: type-safe EDSL for SQL on persistent backends
 esqueleto is a bare bones, type-safe EDSL for SQL queries
 that works with unmodified persistent SQL backends.  Its
 language closely resembles SQL, so you don't have to learn
 new concepts, just new syntax, and it's fairly easy to
 predict the generated SQL and optimize it for your backend.
 Most kinds of errors committed when writing SQL are caught as
 compile-time errors---although it is possible to write
 type-checked esqueleto queries that fail at runtime.

Package: libghc-esqueleto-dev
Architecture: any
Depends: ${haskell:Depends},
 ${shlibs:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-esqueleto-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
